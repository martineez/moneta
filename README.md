## Zadání
Napište program, který bude vystavovat REST službu, která na vstupu bude přijímat text. Služba text transformuje a vrátí zpět.
Transformaci provede tak, že textu otočí pořadí písmen (tedy přečte ho odzadu) s tím, že:

* na pozici, kde se původně vyskytovala písmena a,e,i,o,u budou nově písmena uppercase, všechna ostatní písmena budou lowercase.
* Dvě a více mezer spojí do jedné mezery

Např:

vstup:

    Ahoj, jak se máš?

výstup:

    ?šÁm es kaj ,jOha

vstup:

    Je     mi   fajn.

výstup:

    .NjaF iM ej

Úloha by měla být na cca 1-2 hodin. Úloha by měla umět zpracovat češtinu.

## Vypracování

1. Z popisu není zřejmé, zda dlouhé samohlásky také způsobují uppercase. Vyplývá to však z uvedených příkladů. Řešení tedy zahrnuje i "áéíóůú".
2. Z popisu není dále patrné, v jakém pořadí se má uskutečnit otočení řetězce a vypuštění mezer. Z uvedených příkladů však vyplývá,
že nejprve musí dojít k spojování mezer a pak k otočení řetězce. Řešení tedy nejprve spojuje mezery a pak otočí řetězec.
3. Služba je vystavena na endpointu /transform, metoda GET, parametr text, contentType text/plain. Výsledek je vrácen rovněž v contentType text/plain

# Spuštění
./gradlew bootRun

# Spuštění testů

./gradlew cleanTest test

curl -v -X GET http://localhost:8080/transform?text=Ahoj%2C%20jak%20se%20m%C3%A1%C5%A1%3F

curl -v -X GET http://localhost:8080/transform?text=Je%20%20%20%20%20mi%20%20%20fajn.

# Kontakt

[Martin Fryš](mailto:martin.frys@volny.cz)