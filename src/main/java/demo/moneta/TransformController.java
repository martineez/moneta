package demo.moneta;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController
public class TransformController {

    @Autowired
    private TransformService service;

    public TransformController(TransformService service) {
        this.service = service;
    }

    @GetMapping(value = "/transform", produces = MediaType.TEXT_PLAIN_VALUE)
    public String getTransform(@RequestParam("text") String str) {
        return service.transform(str);
    }
}
