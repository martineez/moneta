package demo.moneta;

import org.springframework.stereotype.Service;

@Service
public class TransformService {

    private boolean isSmallVowel(int c) {
        return "aeiouáéíóůú".indexOf(c) > -1;
    }

    protected String transform(String str) {

        char[] in = str.replaceAll(" +", " ").toCharArray();

        int begin = 0;
        int end = in.length - 1;
        char temp;

        while (end >= begin) {
            temp = in[begin];
            in[begin] = isSmallVowel(in[begin]) ? Character.toUpperCase(in[end]) : Character.toLowerCase(in[end]);
            in[end] = isSmallVowel(in[end]) ? Character.toUpperCase(temp) : Character.toLowerCase(temp);
            end--;
            begin++;
        }

        return new String(in);
    }

}
