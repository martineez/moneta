package demo.moneta;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import static demo.moneta.TestConstants.*;
import static org.junit.Assert.assertEquals;
import static org.springframework.http.HttpMethod.GET;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class TransformHttpRequestTest {

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void transformWithLongVowelsTest() throws Exception {
        String actual = restTemplate.exchange(
                "http://localhost:{port}/transform?text={text}",
                GET,
                null,
                String.class,
                port,
                FIRST_QUERY).getBody();

        assertEquals(FIRST_RESULT, actual);
    }

    @Test
    public void transformWithSpacesTest() throws Exception {
        String actual = restTemplate.exchange(
                "http://localhost:{port}/transform?text={text}",
                GET,
                null,
                String.class,
                port,
                SECOND_QUERY).getBody();

        assertEquals(SECOND_RESULT, actual);
    }

}
