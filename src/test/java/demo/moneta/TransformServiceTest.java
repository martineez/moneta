package demo.moneta;

import org.junit.BeforeClass;
import org.junit.Test;

import static demo.moneta.TestConstants.*;
import static org.junit.Assert.assertEquals;

/**
 * Tests for {@link TransformService}.
 */
public class TransformServiceTest {

    private static TransformService service;

    @BeforeClass
    public static void setup() {
        service = new TransformService();
    }

    @Test
    public void transformWithLongVowels() {
        String actual = service.transform(FIRST_QUERY);

        assertEquals(FIRST_RESULT, actual);
    }

    @Test
    public void transformWithSpaces() {
        String actual = service.transform(SECOND_QUERY);

        assertEquals(SECOND_RESULT, actual);
    }

}
